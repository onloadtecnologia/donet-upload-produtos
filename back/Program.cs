using back.Context;
using Microsoft.Extensions.FileProviders;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
var MyPermitedOrigins="";

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
var connectionString = builder.Configuration.GetConnectionString("database");
builder.Services.AddSqlServer<DataBaseContext>(connectionString);
builder.Services.AddCors(options=>
   options.AddPolicy(MyPermitedOrigins,(builder)=>{
           builder.WithOrigins("*")
           .AllowAnyHeader()
           .AllowAnyOrigin()
           .AllowAnyMethod();
   })
);
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseRouting();
app.UseStaticFiles();
app.UseCors(MyPermitedOrigins);
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
