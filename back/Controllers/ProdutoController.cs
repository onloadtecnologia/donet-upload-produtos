using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using back.Models;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace back.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProdutoController : ControllerBase
    {
        [HttpPost,DisableRequestSizeLimit]
        public async Task<ActionResult<Produto>> produto(IFormFile file){
      
              Produto produto = new Produto();
              try
              {
                string fileName = file.FileName.Split(".")[0]; 
                string filextension = file.FileName.Split(".")[1];
                string arquivo = $"{fileName}{DateTime.Now.Ticks}.{filextension}";

                using(var stream = new FileStream($"./uploads/{arquivo}",FileMode.Create))
                {
                    await file.CopyToAsync(stream);                    
                }
              }
              catch(IOException ex)
              {
                return BadRequest(ex.Message);
              }

              return Ok(produto);  
                        
        }
    }
}