using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using back.Context;
using back.Models;

namespace back.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class ClienteController : Controller
    {

        private readonly DataBaseContext db;


        public ClienteController(DataBaseContext _db)
        {
            db = _db;
        }

        [HttpGet]
        public async Task<ActionResult<List<Cliente>>> findAll()
        {
            var clientes = db.Clientes.ToList();
            return Ok(clientes);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Cliente>> findById(int id)
        {
            var cliente = db.Clientes.ToList().First(i => i.Id == id);
            if (cliente is null)
            {
                return BadRequest("not found");
            }
            return Ok(cliente);
        }

        [HttpPost]
        public async Task<ActionResult<List<Cliente>>> save(Cliente cliente)
        {

            cliente.Data = DateTime.Now;
            db.Add(cliente);
            int rows = await db.SaveChangesAsync();
            if (rows > 0)
            {
                return Created("/cliente", db.Clientes.ToList());
            }
            else
            {
                return BadRequest("erro on server");
            }

        }

        [HttpPut("{id}")]
        public async Task<ActionResult<List<Cliente>>> update(int id, Cliente cliente)
        {
            var oldCliente = await db.Clientes.FindAsync(id);
            if (oldCliente is not null)
            {
                oldCliente.Nome = cliente.Nome;
                oldCliente.Telefone = cliente.Telefone;
                oldCliente.Email = cliente.Email;
                oldCliente.Cep = cliente.Cep;
                await db.SaveChangesAsync();
                return Ok(db.Clientes.ToList());
            }
            else
            {
                return BadRequest("error on server");
            }

        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<List<Cliente>>> delete(int id)
        {
            var delCliente = await db.Clientes.FindAsync(id);
            if (delCliente is not null)
            {
                db.Clientes.Remove(delCliente);
                await db.SaveChangesAsync();
                return Ok(db.Clientes.ToList());
            }
            else
            {
                return BadRequest("Not Found");
            }

        }

    }
}