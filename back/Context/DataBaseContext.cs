using Microsoft.EntityFrameworkCore;
using back.Models;
namespace back.Context;


public class DataBaseContext : DbContext
{

    public DataBaseContext(DbContextOptions options):base(options) { }

    public DbSet<Cliente> Clientes {get; set;} = null;
    public DbSet<Produto> Produtos {get; set;} = null;

}
