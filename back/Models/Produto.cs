using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace back.Models
{
    public class Produto
    {
        public int id {get; set;} = 0;
        public DateTime data {get; set;} = DateTime.Now;
        public string name {get; set;} = string.Empty;
        public string caminho {get; set;} = string.Empty;
    }
}